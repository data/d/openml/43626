# OpenML dataset: Gender-discrimination

https://www.openml.org/d/43626

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Content
A few years ago, the United States District Court of Houston had a case that arises under Title VII of the Civil Rights Act of 1964, 42 U.S.C. 200e et seq. The plaintiffs in this case were all female doctors at Houston College of Medicine who claimed that the College has engaged in a pattern and practice of discrimination against women in giving promotions and setting salaries. The Lead plaintiff in this action, a pediatrician and an assistant professor, was denied for promotion at the College. The plaintiffs had presented a set of data to show that female faculty at the school were less likely to be full professors, more likely to be assistant professors, and earn less money than men, on average.
1 Dept                1=Biochemistry/Molecular Biology
                            2=Physiology
                            3=Genetics
                            4=Pediatrics
                            5=Medicine
                            6=Surgery
2 Gender           1=Male, 0=Female
3 Clin                  1=Primarily clinical emphasis, 0=Primarily research emphasis
4 Cert                 1=Board certified, 0=not certified
5 Prate               Publication rate ( publications on cv)/( years between CV date and MD date)
6 Exper               years since obtaining MD
7 Rank               1=Assistant, 2=Associate, 3=Full professor (a proxy for productivity)
8 Sal94              Salary in academic year 1994
9 Sal95              Salary after increment to 1994
Acknowledgements
We wouldn't be here without the help of others. If you owe any attributions or thanks, include them here along with any citations of past research.
Inspiration
Your data will be in front of the world's largest data science community. What questions do you want to see answered?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43626) of an [OpenML dataset](https://www.openml.org/d/43626). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43626/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43626/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43626/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

